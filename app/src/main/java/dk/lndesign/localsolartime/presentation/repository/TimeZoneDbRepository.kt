package dk.lndesign.localsolartime.presentation.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dk.lndesign.localsolartime.R
import dk.lndesign.localsolartime.data.TimeZoneDbService
import dk.lndesign.localsolartime.data.executeSafely
import dk.lndesign.localsolartime.domain.model.CityTimeZone
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.model.TimeZoneDbRequest
import dk.lndesign.localsolartime.domain.repository.TimeZoneRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TimeZoneDbRepository(val context: Context,
                           private val service: TimeZoneDbService): TimeZoneRepository {

    override fun getTimeZone(latitude: Double, longitude: Double): LiveData<Resource<CityTimeZone>> {
        val mutableLiveData = MutableLiveData<Resource<CityTimeZone>>()

        CoroutineScope(Dispatchers.Main).launch {
            mutableLiveData.value = withContext(Dispatchers.IO) {
                executeSafely(
                    request = { service.getTimeZone(latitude, longitude) },
                    onSuccess = { cityTimeZone ->
                        if (cityTimeZone?.status == TimeZoneDbRequest.Status.SUCCESS) {
                            Resource.success(cityTimeZone)
                        } else {
                            Resource.error(
                                cityTimeZone?.message ?: context.getString(R.string.error_request_time_zone),
                                cityTimeZone
                            )
                        }
                    },
                    onFailed = { errorMessage, cityTimeZone ->
                        Resource.error(
                            errorMessage ?: context.getString(R.string.error_request_time_zone),
                            cityTimeZone
                        )
                    },
                    onException = { error ->
                        Resource.error(
                            error.localizedMessage ?: context.getString(R.string.error_request_time_zone)
                        )
                    }
                )
            }
        }

        return mutableLiveData
    }
}