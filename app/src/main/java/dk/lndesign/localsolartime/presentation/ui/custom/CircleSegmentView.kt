package dk.lndesign.localsolartime.presentation.ui.custom

import android.animation.AnimatorSet
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import timber.log.Timber
import java.util.*

class CircleSegmentView: View {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        post {
//            start()
//        }
    }

    companion object {
        const val PROPERTY_BACKGROUND_ANGLE = "background_angle"
        const val PROPERTY_SUN_POSITION_ANGLE = "sun_position_angle"
        const val PROPERTY_NIGHT_START_ANGLE = "night_start_angle"
        const val PROPERTY_NIGHT_END_ANGLE = "night_end_angle"
    }

    private val circlePaint = Paint().apply {
        setARGB(255, 162, 216, 242)
    }
    private val circleNightPaint = Paint().apply {
        setARGB(255, 227, 227, 227)
    }
    private val sunPositionLinePaint = Paint().apply {
        setARGB(255, 255, 222, 112)
        strokeWidth = 10f
        strokeCap = Paint.Cap.ROUND
    }

    fun start() {
        // Calculate sun position according to current time.
        val sunAngle = getTimePositionDegree(Calendar.getInstance())

        // Create sequential animation of day background and sun position.
        val sequentialAnimatorSet = AnimatorSet()
        sequentialAnimatorSet.playSequentially(
            listOf(
                createBackgroundAnimator(),
                createSunPositionAnimator(sunAngle)
            )
        )
        sequentialAnimatorSet.start()
    }

    private val padding: Float = 30f

    private var animationBackgroundAngle: Float = 0f
    private var animationNightBackgroundAngleStart = 0f
    private var animationSunPositionAngle: Float = 0f
    private var animationNightBackgroundAngleEnd = 180f
    private var sunriseAngle: Float = animationNightBackgroundAngleStart
    private var sunsetAngle: Float = animationNightBackgroundAngleEnd

    private var canvasWidth: Float = 0f
    private var canvasHeight: Float = 0f
    private lateinit var circleOval: RectF

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        // Save canvas size.
        canvasWidth = w.toFloat()
        canvasHeight = h.toFloat()
        // Create bounding box for sun dial.
        circleOval = RectF(padding, padding, canvasWidth - padding, canvasHeight * 2f)
        super.onSizeChanged(w, h, oldw, oldh)
    }

    override fun onDraw(canvas: Canvas?) {
        if (this::circleOval.isInitialized) {
            // Draw day background.
            canvas?.drawArc(
                circleOval,
                180f, animationBackgroundAngle,
                true,
                circlePaint
            )

            // Draw night start background.
            canvas?.drawArc(
                circleOval,
                180f, animationNightBackgroundAngleStart,
                true,
                circleNightPaint
            )
            // Draw night end background.
            canvas?.drawArc(
                circleOval,
                0f, animationNightBackgroundAngleEnd,
                true,
                circleNightPaint
            )

            // Get adjusted sun angle.
            val angle = animationSunPositionAngle - 90f

            // Rotate according to current time.
            canvas?.rotate(angle, canvasWidth / 2f, canvasHeight)
            // Draw sun.
            canvas?.drawLine(canvasWidth / 2f, padding, canvasWidth / 2f, canvasHeight, sunPositionLinePaint)
            canvas?.drawCircle(canvasWidth / 2f, padding, 30f, sunPositionLinePaint)
        }
    }

    /**
     * Calculate position degree from time.
     */
    private fun getTimePositionDegree(calendar: Calendar, totalDegrees: Float = 180f): Float {
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        return (hour * (totalDegrees/24f)) + (minute * ((totalDegrees/24f)/60f))
    }

    private fun startNightAnimation() {
        val parallelAnimatorSet = AnimatorSet()
        parallelAnimatorSet.playTogether(
            createNightStartBackgroundAnimator(),
            createNightEndBackgroundAnimator()
        )
        parallelAnimatorSet.start()
    }

    private fun createBackgroundAnimator(): ValueAnimator {
        val propertyAlpha = PropertyValuesHolder.ofFloat(PROPERTY_BACKGROUND_ANGLE, 0f, 180f)

        val animator = ValueAnimator()
        animator.setValues(propertyAlpha)
        animator.duration = 500
        animator.interpolator = AccelerateDecelerateInterpolator()

        animator.addUpdateListener { animation ->
            animationBackgroundAngle = animation.getAnimatedValue(PROPERTY_BACKGROUND_ANGLE) as Float
            invalidate()
        }

        return animator
    }

    private fun createSunPositionAnimator(degrees: Float): ValueAnimator {
        val propertyAlpha = PropertyValuesHolder.ofFloat(PROPERTY_SUN_POSITION_ANGLE, animationSunPositionAngle, degrees)

        val animator = ValueAnimator()
        animator.setValues(propertyAlpha)
        animator.duration = 500
        animator.interpolator = AccelerateDecelerateInterpolator()

        animator.addUpdateListener { animation ->
            animationSunPositionAngle = animation.getAnimatedValue(PROPERTY_SUN_POSITION_ANGLE) as Float
            invalidate()
        }

        return animator
    }


    private fun createNightStartBackgroundAnimator(): ValueAnimator {
        val propertyAlpha = PropertyValuesHolder.ofFloat(PROPERTY_NIGHT_START_ANGLE, animationNightBackgroundAngleStart, sunriseAngle)

        val animator = ValueAnimator()
        animator.setValues(propertyAlpha)
        animator.duration = 500
        animator.interpolator = AccelerateDecelerateInterpolator()

        animator.addUpdateListener { animation ->
            animationNightBackgroundAngleStart = animation.getAnimatedValue(PROPERTY_NIGHT_START_ANGLE) as Float
            invalidate()
        }

        return animator
    }

    private fun createNightEndBackgroundAnimator(): ValueAnimator {
        val propertyAlpha = PropertyValuesHolder.ofFloat(PROPERTY_NIGHT_END_ANGLE, animationNightBackgroundAngleEnd, sunsetAngle)

        val animator = ValueAnimator()
        animator.setValues(propertyAlpha)
        animator.duration = 500
        animator.interpolator = AccelerateDecelerateInterpolator()

        animator.addUpdateListener { animation ->
            animationNightBackgroundAngleEnd = animation.getAnimatedValue(PROPERTY_NIGHT_END_ANGLE) as Float
            invalidate()
        }

        return animator
    }

    fun updateSunriseSunset(sunrise: Calendar, sunset: Calendar) {
        sunriseAngle = getTimePositionDegree(sunrise)
        sunsetAngle = -180f + getTimePositionDegree(sunset)

        Timber.d("updateSunriseSunset { sunriseAngle: $sunriseAngle, sunsetAngle: $sunsetAngle }")

        startNightAnimation()
    }
}