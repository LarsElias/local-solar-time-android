package dk.lndesign.localsolartime.presentation.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dk.lndesign.localsolartime.R
import dk.lndesign.localsolartime.data.SunriseSunsetAPIService
import dk.lndesign.localsolartime.data.executeSafely
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import dk.lndesign.localsolartime.domain.repository.SunriseSunsetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SunriseSunsetAPIRepository(val context: Context,
                                 private val service: SunriseSunsetAPIService): SunriseSunsetRepository {

    override fun getSunriseSunset(latitude: Double, longitude: Double): LiveData<Resource<SunriseSunset>> {
        val mutableLiveData = MutableLiveData<Resource<SunriseSunset>>()

        CoroutineScope(Dispatchers.Main).launch {
            mutableLiveData.value = withContext(Dispatchers.IO) {
                executeSafely(
                    request = { service.getSunriseSunset(latitude, longitude) },
                    onSuccess = { sunriseSunset ->
                        if (sunriseSunset?.status == SunriseSunset.Status.OK) {
                            Resource.success(sunriseSunset)
                        } else {
                            Resource.error(
                                context.getString(R.string.error_request_sunrise_sunset),
                                sunriseSunset
                            )
                        }
                    },
                    onFailed = { errorMessage, sunriseSunset ->
                        Resource.error(
                            errorMessage ?: context.getString(R.string.error_request_time_zone),
                            sunriseSunset
                        )
                    },
                    onException = { error ->
                        Resource.error(
                            error.localizedMessage ?: context.getString(R.string.error_request_time_zone)
                        )
                    }
                )
            }
        }

        return mutableLiveData
    }
}