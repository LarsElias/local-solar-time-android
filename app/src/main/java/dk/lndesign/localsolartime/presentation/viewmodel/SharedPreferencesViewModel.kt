package dk.lndesign.localsolartime.presentation.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import dk.lndesign.localsolartime.presentation.repository.SharedPreferencesRepository

/**
 * Persistent storage for saving and retrieving values.
 */
class SharedPreferencesViewModel(application: Application): AndroidViewModel(application) {

    // Factory for providing application to view model.
    class SharedPreferencesViewModelFactory(private val application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SharedPreferencesViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SharedPreferencesViewModel(application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    companion object {
        const val PREF_PACKAGE_NAME = "dk.lndesign.localsolartime.preferences"
    }

    private val sharedPreferences = application.getSharedPreferences(PREF_PACKAGE_NAME, Context.MODE_PRIVATE)
    private val sharedPreferencesRepository = SharedPreferencesRepository(sharedPreferences)
}