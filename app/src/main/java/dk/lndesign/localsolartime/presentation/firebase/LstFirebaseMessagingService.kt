package dk.lndesign.localsolartime.presentation.firebase

import com.google.firebase.messaging.FirebaseMessagingService
import timber.log.Timber

class LstFirebaseMessagingService: FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        Timber.i("FCM token refreshed: $token")
    }
}