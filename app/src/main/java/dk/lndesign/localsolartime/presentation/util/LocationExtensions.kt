package dk.lndesign.localsolartime.presentation.util

import android.location.Location

fun Location.getCardinalDirection(): String? {
    return when (this.bearing.toInt()) {
        in 338..360, in 0..23 -> "N"
        in 23..68 -> "NE"
        in 68..113 -> "E"
        in 113..158 -> "SE"
        in 158..203 -> "S"
        in 203..248 -> "SW"
        in 248..293 -> "W"
        in 293..338 -> "NW"
        else -> null
    }
}

fun Location.getSpeedInKmH(): Float {
    // 1 m/s to km/h = 3.6 km/h
    return this.speed * 3.6f
}