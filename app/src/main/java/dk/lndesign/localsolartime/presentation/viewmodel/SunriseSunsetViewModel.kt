package dk.lndesign.localsolartime.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dk.lndesign.localsolartime.LstApp
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import dk.lndesign.localsolartime.domain.usecase.GetSunriseSunsetUseCase

class SunriseSunsetViewModel: ViewModel() {

    private val repository = LstApp.sunriseSunsetRepository

    fun getSunriseSunset(latitude: Double, longitude: Double): LiveData<Resource<SunriseSunset>> =
        GetSunriseSunsetUseCase(repository).execute(GetSunriseSunsetUseCase.Params(latitude, longitude))
}