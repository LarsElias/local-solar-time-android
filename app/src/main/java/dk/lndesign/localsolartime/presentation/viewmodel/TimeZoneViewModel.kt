package dk.lndesign.localsolartime.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dk.lndesign.localsolartime.LstApp
import dk.lndesign.localsolartime.domain.model.CityTimeZone
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.usecase.GetTimeZoneUseCase

class TimeZoneViewModel: ViewModel() {

    private val repository = LstApp.timeZoneRepository

    fun getTimeZone(latitude: Double, longitude: Double): LiveData<Resource<CityTimeZone>> =
        GetTimeZoneUseCase(repository).execute(GetTimeZoneUseCase.Params(latitude, longitude))
}