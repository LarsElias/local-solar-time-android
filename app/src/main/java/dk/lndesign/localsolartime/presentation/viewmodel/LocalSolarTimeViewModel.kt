package dk.lndesign.localsolartime.presentation.viewmodel

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dk.lndesign.localsolartime.domain.usecase.CalculateLocalSolarTimeUseCase
import java.util.*

class LocalSolarTimeViewModel: ViewModel() {

    // Keep instance of use case, so it can cache calculations when location has not changed.
    private val calculateLocalSolarTimeUseCase = CalculateLocalSolarTimeUseCase()

    fun calculateSolarTime(location: Location, timeZone: TimeZone): LiveData<Pair<Date, String?>> =
        calculateLocalSolarTimeUseCase.execute(CalculateLocalSolarTimeUseCase.Params(location, timeZone))
}