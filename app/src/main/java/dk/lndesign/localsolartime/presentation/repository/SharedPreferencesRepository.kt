package dk.lndesign.localsolartime.presentation.repository

import android.content.SharedPreferences
import dk.lndesign.localsolartime.domain.repository.PersistentStorageRepository
import dk.lndesign.localsolartime.presentation.util.observe
import dk.lndesign.localsolartime.presentation.util.set
import kotlinx.coroutines.flow.Flow

class SharedPreferencesRepository(private val sharedPreferences: SharedPreferences): PersistentStorageRepository {

    override fun setValue(key: String, value: Any) {
        sharedPreferences.set(key, value)
    }

    override fun <T> getValue(key: String, defaultValue: T): Flow<T> {
        @Suppress("UNCHECKED_CAST")
        return when (defaultValue) {
            is String -> sharedPreferences.observe(key, defaultValue as String) as Flow<T>
            is Int -> sharedPreferences.observe(key, defaultValue as Int) as Flow<T>
            is Long -> sharedPreferences.observe(key, defaultValue as Long) as Flow<T>
            is Boolean -> sharedPreferences.observe(key, defaultValue as Boolean) as Flow<T>
            is Float -> sharedPreferences.observe(key, defaultValue as Float) as Flow<T>
            is Set<*> -> sharedPreferences.observe(key, defaultValue as Set<*>) as Flow<T>
            is MutableSet<*> -> sharedPreferences.observe(key, defaultValue as MutableSet<*>) as Flow<T>
            else -> sharedPreferences.observe(key, defaultValue as Any) as Flow<T>
        }
    }

}