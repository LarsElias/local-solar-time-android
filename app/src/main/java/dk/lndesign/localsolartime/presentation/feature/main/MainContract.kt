package dk.lndesign.localsolartime.presentation.feature.main

import android.content.Context
import android.location.Address
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import dk.lndesign.localsolartime.presentation.feature.BaseActivityPresenter
import java.util.*

interface MainContract {

    interface Presenter: BaseActivityPresenter<Void> {

        /**
         * Start periodic location update.
         */
        fun startLocationUpdates(context: Context)

        /**
         * Stop location updates.
         */
        fun stopLocationUpdates()

        /**
         * Start clock ticker to update clocks in sync.
         */
        fun startClock()

        /**
         * Stop clock ticker.
         */
        fun stopClock()
    }

    interface View {

        /**
         * Updates loading state of clock.
         */
        fun loading(isLoading: Boolean)

        /**
         * Callback for location permission successfully granted.
         */
        fun onLocationPermissionGranted()

        /**
         * Callback for receiving sunrise/sunset for current location.
         */
        fun onSunriseSunsetReceived(sunriseSunset: SunriseSunset)

        /**
         * Callback on click tick, when clock should be updated.
         */
        fun onClockTick(address: Address, localTime: Calendar, solarTime: Calendar, timeDifference: String?)

        /**
         * An abstract error received.
         */
        fun onError(message: String)
    }
}