package dk.lndesign.localsolartime.presentation.feature.main

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Handler
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import dk.lndesign.localsolartime.BuildConfig
import dk.lndesign.localsolartime.R
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import dk.lndesign.localsolartime.presentation.viewmodel.LocalSolarTimeViewModel
import dk.lndesign.localsolartime.presentation.viewmodel.SharedPreferencesViewModel
import dk.lndesign.localsolartime.presentation.viewmodel.SunriseSunsetViewModel
import dk.lndesign.localsolartime.presentation.viewmodel.TimeZoneViewModel
import timber.log.Timber
import java.util.*

class MainPresenter(private val view: MainContract.View): MainContract.Presenter {

    private lateinit var context: Context
    private lateinit var lifecycleOwner: LifecycleOwner

    private lateinit var sharedPreferencesViewModel: SharedPreferencesViewModel
    private lateinit var timeZoneViewModel: TimeZoneViewModel
    private lateinit var sunriseSunsetModel: SunriseSunsetViewModel
    private lateinit var localSolarTimeViewModel: LocalSolarTimeViewModel

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var geocoder: Geocoder

    private lateinit var handler: Handler
    private lateinit var ticker: Runnable
    private var lastLocation: Location? = null
    private lateinit var timeZone: TimeZone
    private lateinit var address: Address

    companion object {
        private const val LOCATION_REQUEST_U_INTERVAL_DEBUG = 30_000L /* 30 sec */
        private const val LOCATION_REQUEST_F_INTERVAL_DEBUG = 10_000L /* 10 sec */
        private const val LOCATION_REQUEST_U_INTERVAL = 5 * 60_000L /* 5 min */
        private const val LOCATION_REQUEST_F_INTERVAL = 2 * 60_000L /* 2 min */
    }

    override fun attach(activity: FragmentActivity, params: Void?) {
        context = activity
        lifecycleOwner = activity

        // Setup view models.
        sharedPreferencesViewModel = ViewModelProvider(activity, SharedPreferencesViewModel.SharedPreferencesViewModelFactory(activity.application))
            .get(SharedPreferencesViewModel::class.java)
        timeZoneViewModel = ViewModelProvider(activity).get(TimeZoneViewModel::class.java)
        sunriseSunsetModel = ViewModelProvider(activity).get(SunriseSunsetViewModel::class.java)
        localSolarTimeViewModel = ViewModelProvider(activity).get(LocalSolarTimeViewModel::class.java)

        // Setup location update request.
        locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = if (BuildConfig.DEBUG) {
                LOCATION_REQUEST_U_INTERVAL_DEBUG
            } else {
                LOCATION_REQUEST_U_INTERVAL
            }
            fastestInterval = if (BuildConfig.DEBUG) {
                LOCATION_REQUEST_F_INTERVAL_DEBUG
            } else {
                LOCATION_REQUEST_F_INTERVAL
            }
        }
        // Setup location client.
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        // Setup location update callback.
        locationCallback = object: LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                // Get distance from last saved location.
                val metersFromLastLocation = lastLocation?.distanceTo(locationResult.lastLocation)
                Timber.d("Distance from last location: $metersFromLastLocation m")

                // Make sure location has changed a significant amount.
                // Only fetch location information when location has changed more than 200 meters,
                // or if it has not been set yet.
                if (metersFromLastLocation == null || metersFromLastLocation > 200) {
                    Timber.i("Location changed by $metersFromLastLocation meters from last location, updating location name and time zone")

                    // Set location and update location information.
                    updateLocation(locationResult.lastLocation)
                } else {
                    Timber.i("Location has not changed significantly, skipping update of location name and time zone")
                }
            }
        }

        // Setup geo coder for fetching address.
        geocoder = Geocoder(context, Locale.getDefault())

        // Setup handler and repeating task for clock ticker.
        handler = Handler()
        ticker = object: Runnable {
            override fun run() {
                try {
                    val location = lastLocation
                    val isLocationAvailable = location != null &&
                            this@MainPresenter::timeZone.isInitialized &&
                            this@MainPresenter::address.isInitialized

                    // Update loading state.
                    view.loading(!isLocationAvailable)

                    // Tick clock if location is available.
                    if (isLocationAvailable && location != null) {
                        localSolarTimeViewModel.calculateSolarTime(location, timeZone).observe(lifecycleOwner, Observer { result ->
                            val localSolarTime: Date = result.first
                            val timeDifference: String? = result.second

                            val localTime = Calendar.getInstance().apply {
                                // Make sure to apply time zone from location.
                                timeZone = this@MainPresenter.timeZone
                                time = Date()
                            }
                            val solarTime = Calendar.getInstance().apply {
                                // Calculate local solar time and set corrected time.
                                time = localSolarTime
                            }

                            view.onClockTick(address, localTime, solarTime, timeDifference)
                        })
                    }
                } finally {
                    // Make sure to repeat delayed task, even when exception is thrown.
                    handler.postDelayed(this, 1000)
                }
            }
        }
    }

    override fun startLocationUpdates(context: Context) {
        // Check if location permission has been granted, so we can get location for clock.
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // Get location to find time zone and location information.
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
        }
    }

    override fun stopLocationUpdates() {
        if (this::fusedLocationClient.isInitialized) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    override fun startClock() {
        ticker.run()
    }

    override fun stopClock() {
        handler.removeCallbacks(ticker)
    }

    /**
     * Set location and update location information; location name, time zone and subset/sunrise time.
     * Location is set for next location update.
     */
    private fun updateLocation(location: Location) {
        lastLocation = location

        // Get location information.
        // Get location name through address lookup.
        getAddress(location.latitude, location.longitude)
        // Get timezone from location.
        getTimeZone(location.latitude, location.longitude)

        // Get sunrise/sunset for current location.
        getSunriseSunset(location.latitude, location.longitude)
    }

    /**
     * Get time zone from location.
     */
    private fun getTimeZone(latitude: Double, longitude: Double) {
        // Get time zone of location.
        timeZoneViewModel.getTimeZone(latitude, longitude).observe(lifecycleOwner, Observer { resource ->
            // Get time zone from resource or get device locale as default.
            this.timeZone = when (resource.status) {
                Resource.Status.SUCCESS -> {
                    val cityTimeZone = resource.data
                    if (cityTimeZone != null) {
                        // Get time zone from city location.
                        TimeZone.getTimeZone(cityTimeZone.zoneName)
                    } else {
                        Timber.w(context.getString(R.string.error_request_time_zone))

                        // Use device time zone.
                        TimeZone.getDefault()
                    }
                }
                Resource.Status.ERROR -> {
                    Timber.w(resource.errorMessage ?: context.getString(R.string.error_request_time_zone))

                    // Use device time zone.
                    TimeZone.getDefault()
                }
            }
        })
    }

    /**
     * Get address from location.
     */
    private fun getAddress(latitude: Double, longitude: Double) {
        try {
            geocoder.getFromLocation(latitude, longitude, 1)
                .firstOrNull()
                ?.let { address ->
                    this.address = address
                }
        } catch (e: Exception) {
            Timber.w(e,"Could not get address for location; latitude: $latitude, longitude: $longitude")
        }
    }

    private fun getSunriseSunset(latitude: Double, longitude: Double) {
        sunriseSunsetModel.getSunriseSunset(latitude, longitude).observe(lifecycleOwner, Observer { resource ->
            when (resource.status) {
                Resource.Status.SUCCESS -> {
                    val sunriseSunset: SunriseSunset? = resource.data
                    if (sunriseSunset != null) {
                        view.onSunriseSunsetReceived(sunriseSunset)
                    } else {
                        view.onError(context.getString(R.string.error_request_sunrise_sunset))
                    }
                }
                Resource.Status.ERROR -> {
                    view.onError(resource.errorMessage ?: context.getString(R.string.error_request_sunrise_sunset))
                }
            }
        })
    }
}