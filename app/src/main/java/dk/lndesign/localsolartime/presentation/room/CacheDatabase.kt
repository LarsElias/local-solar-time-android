package dk.lndesign.localsolartime.presentation.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import dk.lndesign.localsolartime.data.dao.CacheEntryDao
import dk.lndesign.localsolartime.data.model.CacheEntry

@Database(version = 1, entities = [ CacheEntry::class ])
@TypeConverters(Converters::class)
abstract class CacheDatabase: RoomDatabase() {
    abstract fun cacheEntryDao(): CacheEntryDao
}