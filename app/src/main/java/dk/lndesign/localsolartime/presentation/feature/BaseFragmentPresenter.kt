package dk.lndesign.localsolartime.presentation.feature

import androidx.fragment.app.Fragment

/**
 * Base presenter for fragments.
 */
interface BaseFragmentPresenter<in Params: Any?> {

    fun attach(fragment: Fragment, params: Params? = null)
}