package dk.lndesign.localsolartime.presentation.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dk.lndesign.localsolartime.R
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import dk.lndesign.localsolartime.presentation.feature.main.MainContract
import dk.lndesign.localsolartime.presentation.feature.main.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.text.DateFormat
import java.util.*

class MainActivity: AppCompatActivity(), MainContract.View {

    companion object {

        private const val PERMISSION_REQUEST_LOCATION = 1

        /**
         * Get intent to start main activity.
         * The new activity will become the new stack root, clearing any previous back history.
         */
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
        }
    }

    private lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)
        presenter.attach(this)

        // Specifically request missing location permission on create.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Request permissions.
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_LOCATION)
        }
    }

    override fun onResume() {
        super.onResume()

        // Check if location permissions have been granted.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // Make sure overlay for missing permission is not shown, if they have been granted.
            grantMissingPermissionLayout.visibility = View.GONE
        }

        presenter.startClock()

        // Update location.
        presenter.startLocationUpdates(this)

        circleSegmentView.post {
            circleSegmentView.start()
        }
    }

    override fun onPause() {
        super.onPause()

        presenter.stopClock()

        presenter.stopLocationUpdates()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_LOCATION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Timber.d("Location permission granted")

                    // Hide missing permission text.
                    grantMissingPermissionLayout.visibility = View.GONE
                } else {
                    Timber.w("Location permission denied")

                    // Show text for missing location permission.
                    grantMissingPermissionLayout.visibility = View.VISIBLE
                    // Get location permission when clicking text.
                    grantMissingPermissionButton.setOnClickListener {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            // Ask for permission when user clicks text.
                            MaterialAlertDialogBuilder(this)
                                .setMessage(R.string.location_permission_rationale)
                                .setPositiveButton(R.string.ok) { _, _ ->
                                    // Request permissions.
                                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_LOCATION)
                                }
                                .create()
                                .show()
                        } else {
                            // Let user know that they need to grant permission from app settings.
                            MaterialAlertDialogBuilder(this)
                                .setMessage(R.string.location_permission_rationale)
                                .setPositiveButton(R.string.ok) { _, _ ->
                                    // Go to app settings.
                                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                                        data = Uri.fromParts("package", packageName, null)
                                    }
                                    startActivity(intent)
                                }
                                .create()
                                .show()
                        }
                    }
                }
                return
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun loading(isLoading: Boolean) {
        // Views with loading skeleton.
        val loadingSkeletonViews: List<View> = listOf(
            clockLocationName,
            localDateLabel,
            localTimeClockLayout,
            timeDifferenceLabel,
            solarTimeClockLayout,
            solarDateLabel
        )
        // Dividers in clock, which are hidden when skeleton is shown.
        val timeDividers: List<TextView> = listOf(
            localTimeDivider1, localTimeDivider2,
            solarTimeDivider1, solarTimeDivider2
        )

        // Show skeleton if current loading.
        loadingSkeletonViews.forEach { view ->
            view.background = if (isLoading) {
                ContextCompat.getDrawable(this, R.drawable.skeleton)
            } else {
                null
            }
        }

        // Hide time unit dividers if currently loading.
        timeDividers.forEach { textView ->
            textView.text = if (isLoading) {
                null
            } else {
                getString(R.string.clock_unit_separator)
            }
        }
    }

    override fun onLocationPermissionGranted() {
        // Hide missing permission text.
        grantMissingPermissionLayout.visibility = View.GONE
    }

    override fun onSunriseSunsetReceived(sunriseSunset: SunriseSunset) {
        Timber.d("onSunriseSunsetReceived { sunrise: ${sunriseSunset.result.sunrise}, sunset: ${sunriseSunset.result.sunset} }")

        circleSegmentView.updateSunriseSunset(
            Calendar.getInstance().apply {
                time = sunriseSunset.result.sunrise
            },
            Calendar.getInstance().apply {
                time = sunriseSunset.result.sunset
            }
        )
    }

    override fun onClockTick(address: Address, localTime: Calendar, solarTime: Calendar, timeDifference: String?) {
        // Get location name from address fields describing area.
        val locationName = listOfNotNull(
            address.locality,
            address.countryName
        ).joinToString()

        Timber.d("onClockTick { localTime: ${localTime.time}, solarTime: ${solarTime.time}, locationName: $locationName }")

        clockLocationName.text = locationName

        if (localTime.get(Calendar.DAY_OF_YEAR) != solarTime.get(Calendar.DAY_OF_YEAR)) {
            val dateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault())
            localDateLabel.apply {
                text = dateFormatter.format(localTime.time)
                visibility = View.VISIBLE
            }
            solarDateLabel.apply {
                text = dateFormatter.format(solarTime.time)
                visibility = View.VISIBLE
            }
        } else {
            localDateLabel.apply {
                text = null
                visibility = View.GONE
            }
            solarDateLabel.apply {
                text = null
                visibility = View.GONE
            }
        }

        localTimeClockHours.text = localTime.get(Calendar.HOUR_OF_DAY).toString().padStart(2, '0')
        localTimeClockMinutes.text = localTime.get(Calendar.MINUTE).toString().padStart(2, '0')
        localTimeClockSeconds.text = localTime.get(Calendar.SECOND).toString().padStart(2, '0')

        solarTimeClockHours.text = solarTime.get(Calendar.HOUR_OF_DAY).toString().padStart(2, '0')
        solarTimeClockMinutes.text = solarTime.get(Calendar.MINUTE).toString().padStart(2, '0')
        solarTimeClockSeconds.text = solarTime.get(Calendar.SECOND).toString().padStart(2, '0')

        timeDifferenceLabel.apply {
            text = timeDifference
        }
    }

    override fun onError(message: String) {
        Timber.w("onError { message: $message }")
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
