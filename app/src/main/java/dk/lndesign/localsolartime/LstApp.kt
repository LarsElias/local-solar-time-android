package dk.lndesign.localsolartime

import android.app.Application
import androidx.room.Room
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import dk.lndesign.localsolartime.data.SunriseSunsetAPIService
import dk.lndesign.localsolartime.data.TimeZoneDbService
import dk.lndesign.localsolartime.domain.repository.SunriseSunsetRepository
import dk.lndesign.localsolartime.domain.repository.TimeZoneRepository
import dk.lndesign.localsolartime.presentation.helper.FlipperClientInitializer
import dk.lndesign.localsolartime.presentation.repository.SunriseSunsetAPIRepository
import dk.lndesign.localsolartime.presentation.repository.TimeZoneDbRepository
import dk.lndesign.localsolartime.presentation.room.CacheDatabase
import dk.lndesign.localsolartime.presentation.util.isNetworkAvailable
import okhttp3.OkHttpClient
import timber.log.Timber

class LstApp: Application() {

    companion object {

        // Caching database.
        lateinit var cacheDatabase: CacheDatabase

        // Time zone API repository.
        private lateinit var timeZoneOkHttpClient: OkHttpClient
        lateinit var timeZoneRepository: TimeZoneRepository

        // Sunrise/sunset API repository.
        private lateinit var sunriseSunsetOkHttpClient: OkHttpClient
        lateinit var sunriseSunsetRepository: SunriseSunsetRepository
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())

            // Subscribe to debug topic.
            FirebaseMessaging.getInstance().subscribeToTopic("debug")

            // Get FCM token.
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Timber.w(task.exception, "Failed to get Firebase instance id")
                        return@OnCompleteListener
                    }

                    val token = task.result?.token
                    Timber.d("FCM token: $token")
                })
        }

        // Setup Flipper debugging.
        val flipperInitializer = FlipperClientInitializer()
        flipperInitializer.start(this)

        // Create local database for caching.
        cacheDatabase = Room
            .databaseBuilder(applicationContext, CacheDatabase::class.java, "lst-cache-database")
            // Wipe and rebuild database instead of migration.
            .fallbackToDestructiveMigration()
            .build()

        // API services.
        timeZoneOkHttpClient = TimeZoneDbService.createOkHttpClient(
            BuildConfig.TIME_ZONE_DB_API_KEY,
            this.cacheDir,
            flipperInitializer.getDebugNetworkInterceptor(),
            { this.isNetworkAvailable() }
        ).build()
        timeZoneRepository = TimeZoneDbRepository(
            this,
            TimeZoneDbService.createService(timeZoneOkHttpClient)
        )

        sunriseSunsetOkHttpClient = SunriseSunsetAPIService.createOkHttpClient(
            this.cacheDir,
            flipperInitializer.getDebugNetworkInterceptor(),
            { this.isNetworkAvailable() }
        ).build()
        sunriseSunsetRepository = SunriseSunsetAPIRepository(
            this,
            SunriseSunsetAPIService.createService(sunriseSunsetOkHttpClient)
        )
    }
}