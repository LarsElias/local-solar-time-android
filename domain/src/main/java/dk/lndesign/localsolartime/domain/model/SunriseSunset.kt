package dk.lndesign.localsolartime.domain.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class SunriseSunset(@SerializedName("results") val result: SunriseSunsetResult,
                         val status: Status) {
    data class SunriseSunsetResult(
        val sunrise: Date,
        val sunset: Date
    )
    enum class Status(val value: String) {
        @SerializedName("OK")
        OK("OK"),

        @SerializedName("INVALID_REQUEST")
        INVALID_REQUEST("INVALID_REQUEST"),

        @SerializedName("INVALID_DATE")
        INVALID_DATE("INVALID_DATE"),

        @SerializedName("UNKNOWN_ERROR")
        UNKNOWN_ERROR("UNKNOWN_ERROR")
    }
}