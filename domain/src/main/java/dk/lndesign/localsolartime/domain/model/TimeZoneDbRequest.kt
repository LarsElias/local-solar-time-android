package dk.lndesign.localsolartime.domain.model

import com.google.gson.annotations.SerializedName

abstract class TimeZoneDbRequest {

    enum class Status(val value: String) {
        @SerializedName("SUCCESS")
        SUCCESS("SUCCESS"),

        @SerializedName("FAILED")
        FAILED("FAILED")
    }

    abstract val status: Status
    abstract val message: String
}