package dk.lndesign.localsolartime.domain.model

import com.google.gson.annotations.SerializedName

data class CityTimeZone(
    override val status: Status,
    override val message: String,
    val cityName: String,
    val countryName: String,
    val regionName: String,
    val zoneName: String,
    val abbreviation: String,
    val gmtOffset: Int,
    val dst: DaylightSavingTime
): TimeZoneDbRequest() {
    enum class DaylightSavingTime(val value: String) {
        @SerializedName("1")
        YES("1"),

        @SerializedName("0")
        NO("0"),

        @SerializedName("-1")
        UNDEFINED("-1")
    }
}