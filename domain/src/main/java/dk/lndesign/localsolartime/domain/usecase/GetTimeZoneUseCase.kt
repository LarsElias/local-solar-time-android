package dk.lndesign.localsolartime.domain.usecase

import androidx.lifecycle.LiveData
import dk.lndesign.localsolartime.domain.interactor.LiveDataInteractor
import dk.lndesign.localsolartime.domain.model.CityTimeZone
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.repository.TimeZoneRepository

class GetTimeZoneUseCase(private val repository: TimeZoneRepository): LiveDataInteractor<Resource<CityTimeZone>, GetTimeZoneUseCase.Params>() {

    override fun build(params: Params?): LiveData<Resource<CityTimeZone>> {
        val state = checkNotNull(params) { "Params must be provided" }
        return repository.getTimeZone(state.latitude, state.longitude)
    }

    data class Params(val latitude: Double, val longitude: Double)
}