package dk.lndesign.localsolartime.domain.repository

import kotlinx.coroutines.flow.Flow

interface PersistentStorageRepository {

    /**
     * Set value on persistent storage by key.
     */
    fun setValue(key: String, value: Any)

    /**
     * Get value from persistent storage by key.
     */
    fun <T> getValue(key: String, defaultValue: T): Flow<T>
}