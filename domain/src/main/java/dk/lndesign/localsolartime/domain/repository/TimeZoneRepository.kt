package dk.lndesign.localsolartime.domain.repository

import androidx.lifecycle.LiveData
import dk.lndesign.localsolartime.domain.model.CityTimeZone
import dk.lndesign.localsolartime.domain.model.Resource

interface TimeZoneRepository {

    /**
     * Get time zone for location.
     */
    fun getTimeZone(latitude: Double, longitude: Double): LiveData<Resource<CityTimeZone>>
}