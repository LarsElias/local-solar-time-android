package dk.lndesign.localsolartime.domain.usecase

import androidx.lifecycle.LiveData
import dk.lndesign.localsolartime.domain.interactor.LiveDataInteractor
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import dk.lndesign.localsolartime.domain.repository.SunriseSunsetRepository

class GetSunriseSunsetUseCase(private val repository: SunriseSunsetRepository): LiveDataInteractor<Resource<SunriseSunset>, GetSunriseSunsetUseCase.Params>() {

    override fun build(params: Params?): LiveData<Resource<SunriseSunset>> {
        val state = checkNotNull(params) { "Params must be provided" }
        return repository.getSunriseSunset(state.latitude, state.longitude)
    }

    data class Params(val latitude: Double, val longitude: Double)
}