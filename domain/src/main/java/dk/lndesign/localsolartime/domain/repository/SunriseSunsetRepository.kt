package dk.lndesign.localsolartime.domain.repository

import androidx.lifecycle.LiveData
import dk.lndesign.localsolartime.domain.model.Resource
import dk.lndesign.localsolartime.domain.model.SunriseSunset

interface SunriseSunsetRepository {

    /**
     * Get sunrise/sunset for location.
     */
    fun getSunriseSunset(latitude: Double, longitude: Double): LiveData<Resource<SunriseSunset>>
}