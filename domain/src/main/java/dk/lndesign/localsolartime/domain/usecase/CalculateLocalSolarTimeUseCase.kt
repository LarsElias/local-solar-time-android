package dk.lndesign.localsolartime.domain.usecase

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dk.lndesign.localsolartime.domain.interactor.LiveDataInteractor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.math.*

/**
 * https://www.pveducation.org/pvcdrom/properties-of-sunlight/solar-time
 * https://forum.arduino.cc/index.php?topic=227196.0
 */
class CalculateLocalSolarTimeUseCase: LiveDataInteractor<Pair<Date, String?>, CalculateLocalSolarTimeUseCase.Params>() {

    private var lastLocation: Location? = null
    private var timeCorrection: Double? = null

    override fun build(params: Params?): LiveData<Pair<Date, String?>> {
        val state = checkNotNull(params) { "Params must be provided" }
        val mutableLiveData = MutableLiveData<Pair<Date, String?>>()

        CoroutineScope(Dispatchers.Main).launch {
            mutableLiveData.value = withContext(Dispatchers.IO) {
                // Get distance from last location.
                val metersFromLastLocation = lastLocation?.distanceTo(state.location)

                // Only do calculations if location has changed significantly.
                if (metersFromLastLocation == null || metersFromLastLocation > 200 || timeCorrection == null) {
                    lastLocation = state.location

                    timeCorrection = timeCorrection(
                        localStandardTimeMeridian(state.timeZone),
                        equationOfTime(b()),
                        state.location.longitude
                    )
                }

                // println("timeCorrection: $timeCorrection")

                val timeDifference = timeCorrection?.let {
                    val timeCorrectionTotalSeconds = floor(it * 60).toInt()

                    val sign = when {
                        it.sign.toInt() < 0 -> "-"
                        else -> "+"
                    }
                    val hours = timeCorrectionTotalSeconds.div(60).div(60).rem(60)
                    val minutes = timeCorrectionTotalSeconds.div(60).rem(60)
                    val seconds = timeCorrectionTotalSeconds.rem(60)

                    "$sign ${hours.toTimeUnitString()}h ${minutes.toTimeUnitString()}m ${seconds.toTimeUnitString()}s"
                }

                val localSolarTime = Calendar.getInstance().apply {
                    timeCorrection?.let {
                        // Calculate time correction in seconds and adjust the time.
                        val timeCorrectionTotalSeconds = floor(it * 60).toInt()
                        add(Calendar.SECOND, timeCorrectionTotalSeconds)
                    }
                }

                // Return calculated local solar time and time difference.
                Pair(
                    localSolarTime.time,
                    timeDifference
                )
            }
        }

        return mutableLiveData
    }

    data class Params(
        val location: Location,
        val timeZone: TimeZone
    )

    private fun localStandardTimeMeridian(timeZone: TimeZone): Int {
        val calendar = Calendar.getInstance(timeZone)
        val offsetInHours = timeZone.getOffset(calendar.time.time) / 1000 / 60 / 60

        // int LSTM = 15 * timezone_offset_from_GMT;
        return 15 * offsetInHours
    }

    private fun b(): Double {
        // float B = ((2 * 3.1416) / 365) * (doy - 81);
        val b = ((2 * PI) / 365) * (dayOfYear() - 81)
        return b
    }

    private fun equationOfTime(b: Double): Double {
        // float EoT = 9.87 * sin(2 * B) - 7.53 * cos(B) - 1.5 * sin(B);
        return 9.87 * sin(2 * b) - 7.53 * cos(b) - 1.5 * sin(b)
    }

    private fun timeCorrection(localStandardTimeMeridian: Int, equationOfTime: Double, longitude: Double): Double {
        // float TC = 4 * (longitude - LSTM) + EoT;
        return 4 * (longitude - localStandardTimeMeridian) + equationOfTime
    }

    private fun dayOfYear(): Int {
        return Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
    }

    private fun Int.toTimeUnitString(): String {
        return abs(this).toString()//.padStart(2, '0')
    }
}