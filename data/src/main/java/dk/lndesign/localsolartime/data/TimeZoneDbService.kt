package dk.lndesign.localsolartime.data

import com.google.gson.GsonBuilder
import dk.lndesign.localsolartime.data.interceptor.AuthenticationInterceptor
import dk.lndesign.localsolartime.domain.model.CityTimeZone
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.File
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

interface TimeZoneDbService {

    companion object {

        // Base URL of service.
        private const val BASE_URL = "https://api.timezonedb.com/v2.1/"

        /**
         * Create client for timezonedb.com.
         */
        fun createOkHttpClient(apiKey: String,
                               cacheFile: File?,
                               debugNetworkInterceptor: Interceptor?,
                               isNetworkAvailable: () -> Boolean,
                               noNetworkErrorMessage: String? = "Network connection not available",
                               cacheMaxSize: Long = 15 * 1024 * 1024): OkHttpClient.Builder {

            return OkHttpClient.Builder().apply {

                // Add authentication key.
                addInterceptor(AuthenticationInterceptor(apiKey))

                // Logging.
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BASIC
                })

                // Interceptor for checking if network connection is available.
                addInterceptor { chain ->
                    if (!isNetworkAvailable()) {
                        throw UnknownHostException(noNetworkErrorMessage)
                    }
                    chain.proceed(chain.request())
                }

                // Add interceptor for Flipper debugging tool.
                if (debugNetworkInterceptor != null) {
                    addInterceptor(debugNetworkInterceptor)
                }

                // Optional caching.
                cache(cacheFile?.let { Cache(it, cacheMaxSize) })

                // Timeouts for long running requests.
                connectTimeout(60, TimeUnit.SECONDS)
                readTimeout(60, TimeUnit.SECONDS)
            }
        }

        /**
         * Create service for timezonedb.com.
         */
        fun createService(okHttpClient: OkHttpClient): TimeZoneDbService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder().create()
                    )
                )
                .build()

            return retrofit.create(TimeZoneDbService::class.java)
        }
    }

    /**
     * Get time zone for location.
     */
    @GET("get-time-zone?format=json&by=position")
    suspend fun getTimeZone(@Query("lat") latitude: Double, @Query("lng") longitude: Double): Response<CityTimeZone>
}