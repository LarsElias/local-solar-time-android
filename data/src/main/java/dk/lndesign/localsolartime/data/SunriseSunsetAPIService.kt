package dk.lndesign.localsolartime.data

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import dk.lndesign.localsolartime.domain.model.SunriseSunset
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.File
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

interface SunriseSunsetAPIService {

    companion object {

        // Base URL of service.
        private const val BASE_URL = "https://api.sunrise-sunset.org/"

        // Date format used in service.
        private const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        /**
         * Create service for api.sunrise-sunset.org.
         */
        fun createOkHttpClient(cacheFile: File?,
                               debugNetworkInterceptor: Interceptor?,
                               isNetworkAvailable: () -> Boolean,
                               noNetworkErrorMessage: String? = "Network connection not available",
                               cacheMaxSize: Long = 15 * 1024 * 1024): OkHttpClient.Builder {

            return OkHttpClient.Builder().apply {

                // Logging.
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BASIC
                })

                // Interceptor for checking if network connection is available.
                addInterceptor { chain ->
                    if (!isNetworkAvailable()) {
                        throw UnknownHostException(noNetworkErrorMessage)
                    }
                    chain.proceed(chain.request())
                }

                // Add interceptor for Flipper debugging tool.
                if (debugNetworkInterceptor != null) {
                    addInterceptor(debugNetworkInterceptor)
                }

                // Optional caching.
                cache(cacheFile?.let { Cache(it, cacheMaxSize) })

                // Timeouts for long running requests.
                connectTimeout(60, TimeUnit.SECONDS)
                readTimeout(60, TimeUnit.SECONDS)
            }
        }

        fun createService(okHttpClient: OkHttpClient): SunriseSunsetAPIService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder()
                            // Automatic parsing of date strings into date objects.
                            .setDateFormat(DATE_FORMAT)
                            // Automatic parsing of field naming for lowercase with underscores.
                            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                            .create()
                    )
                )
                .build()

            return retrofit.create(SunriseSunsetAPIService::class.java)
        }
    }

    /**
     * Get sunrise/sunset for location.
     */
    @GET("json")
    suspend fun getSunriseSunset(@Query("lat") latitude: Double,
                                 @Query("lng") longitude: Double,
                                 @Query("date") date: String = "today",
                                 @Query("formatted") formatted: Int = 0): Response<SunriseSunset>
}