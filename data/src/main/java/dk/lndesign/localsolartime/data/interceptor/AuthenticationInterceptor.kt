package dk.lndesign.localsolartime.data.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Interceptor for authentication.
 */
class AuthenticationInterceptor(private val apiKey: String): Interceptor {

    companion object {
        private const val AUTH_QUERY_KEY = "key"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        return chain.proceed(
            originalRequest.newBuilder()
                .url(
                    originalRequest.url.newBuilder()
                        .addQueryParameter(AUTH_QUERY_KEY, apiKey)
                        .build()
                )
                .build()
        )
    }

}