![local-solar-time](app/src/main/res/mipmap-xxxhdpi/ic_launcher.png)

# Local Solar Time

Clock using your current location to [display local time](https://en.wikipedia.org/wiki/Solar_time).

Available on Google Play Store: https://play.google.com/store/apps/details?id=dk.lndesign.localsolartime


- - -

## Setup

### Signing key and credentials

Add new `keystore.jks` and `keystore.properties` in project root folder:

```
storeFile=../keystore.jks
storePassword=...
keyAlias=...
keyPassword=...
```

### Firebase

Add a `google-services.json` config file from a new Firebase project to enable crash reporting and analytics.

### Time Zone DB

Add API key in local.properties from timezonedb.com/register:

```
timeZoneDbApiKey=...
```

## TODO

[ ] Get local time from time zone

[x] Add sunrise / sunset

[x] Add sunlight graph

[x] Add sun position

[ ] Manual location input

[x] Crash reporting

[X] Loading skeleton

[ ] Android widget

[ ] Offline mode

[ ] Write more tests


- - -

## Flipper for Android - debugging database, SharedPreferences and requests

To debug SharedPreferences, SQLite databases, layout or network traffic, download and run Flipper: https://fbflipper.com/

Make sure to run your application as debug variant, as it is disabled in the release variant.


- - -

##  Credits

PVEducation on solar time: https://www.pveducation.org/pvcdrom/properties-of-sunlight/solar-time

TimeZoneDB: https://timezonedb.com/

Sunrise Sunset: https://sunrise-sunset.org/api

Online Web Fonts: http://www.onlinewebfonts.com

SchemeColor: https://www.schemecolor.com/

## License

![CC BY-NC-SA 4.0](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).
